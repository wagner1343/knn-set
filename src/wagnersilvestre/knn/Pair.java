package wagnersilvestre.knn;

public class Pair<K, V> {
    public Pair(K k, V v) {
        this.k = k;
        this.v = v;
    }

    public K k;
    public V v;
}
